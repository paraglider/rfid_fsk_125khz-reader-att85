// v5

#include <stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "ssd1306xled.h"


//  ATtiny85 Pinout
//    o----------+
//  --+ PB5  Vcc +--
//  --+ PB3  PB2 +--
//  --+ PB4  PB1 +--
//  --+ GND  PB0 +--
//    +----------+

#define LED_PIN             PB0
#define PWM_PIN             PB1
#define PULSE_SIGNAL        PB2
#define SSD1306_SCL         PB3         // SCL (ssd1306xled library)
#define SSD1306_SDA         PB4         // SDA (ssd1306xled library)
#define SSD1306_SA          0x78        // OLED address (ssd1306xled library)
#define TX                  PB3         // UART bitbanging
#define ONE_BIT_DELAY       104         // UART bitbanging

#define maxbuf 90                       // buffer length (uint_8t)
uint8_t data[maxbuf];                   // signal buffer array
uint8_t ind;                            // buffer array index
uint8_t cur_state;                      // current signal pin state (for rising edge detection)
uint8_t prev_state;                     // previous signal pin state (for rising edge detection)
uint8_t pulse_count;                    // pulse counter
uint8_t bit_count = 1;                  // repeated bit counter
uint8_t cur_bit;                        // current bit
uint8_t prev_bit;                       // previous bit
uint8_t acquisition_flag;               // seek starting sequence (0) or record data (1)


void clk_div_1(void) {                  // Set clock divider to 1 (fast operation)
    uint8_t sreg = SREG;                // save SREG state
    cli();
    CLKPR = (1<<CLKPCE);                // initiate change
    CLKPR = 0x00;                       // div 1
    SREG = sreg;                        // restore state
}

void clk_div_8(void) {                  // Set clock divider to 8 (slow operation)
    uint8_t sreg = SREG;                // save SREG state
    cli();
    CLKPR = (1<<CLKPCE);                // initiate change
    CLKPR = (1<<CLKPS1)|(1<<CLKPS0);    // div 8
    SREG = sreg;                        // restore state
}

void reset(void) {                      // start new buffer, reset all parameters
    ind = 0;
    bit_count = 1;
    pulse_count = 1;
    cur_state = 0;
    prev_state = 0;
    prev_bit = 0;
    acquisition_flag = 0;
}

/* UART bitbanging for debugging
void tx_uart (uint8_t ascii) {
    PORTB &= ~(1 << TX);
    _delay_us(ONE_BIT_DELAY);
    for(uint8_t i=0; i<8; i++) {
        if ((ascii>>i)&1)       PORTB |= (1 << TX);
        else                    PORTB &= ~(1 << TX);
        _delay_us(ONE_BIT_DELAY);
    }
    PORTB |= (1 << TX);
    _delay_us(ONE_BIT_DELAY);
}

void output_uart(uint8_t max) {
    for(uint8_t i = 0; i < max; i++)  tx_uart (data[i]);
} */

void pwm(uint8_t flag) {                                    // PWM and interrupt enable/disable
    if (flag) {
        TIFR |= 1<<TOV1;                                    // clear outstanding interrupt flag if any
        TIMSK = 1<<TOIE1;                                   // interrupt on timer overflow
        DDRB |= 1<<PWM_PIN;
        sei();
    }
    else {
        cli();
        DDRB &= ~(1<<PWM_PIN);
        TIMSK &= ~(1<<TOIE1);                               // interrupt on timer overflow off
        TIFR |= 1<<TOV1;                                    // clear outstanding interrupt flag if any
    }
}

// TEST NOTES
// The interrupt timings are good: max 3.5 µs per interrupt (usually 1 to 3)
// At 127 kHz there are 7.8 µs available per interrupt

ISR(TIMER1_OVF_vect) {                                          // compare match interrupt
    cur_state = (PINB & (1<<PULSE_SIGNAL));                     // read op-amp output
    pulse_count++;
    if(cur_state != prev_state) {                               // transition (low to high or high to low)
        prev_state = cur_state;
        if(cur_state) {                                         // got cycle start (low to high transition)
            if (acquisition_flag) {                             // collecting data
                if (ind<(maxbuf-1)) {                           // process first 89 bits
                    if ((pulse_count==11)||(pulse_count==12)) {                             // current bit is 1
                        if (prev_bit==1)                              bit_count++;          // previous bit was 1
                        else {                                                              // previous bit was 0
                            if((bit_count>=3)&&(bit_count<=8))        data[ind++] = 0;                      // single bit
                            else if((bit_count>=9)&&(bit_count<=14))  {data[ind++] = 0; data[ind++] = 0;}   // double bit
                            else    {
                                //data[ind++] = 3;                                          // debugging error code 3
                                //data[ind++] = bit_count;                                  // debugging
                                reset();
                            }
                            prev_bit = 1;                                                   // change to current state
                            bit_count = 1; } }
                    else if ((pulse_count==8)||(pulse_count==9)) {                          // current bit is 0
                        if (prev_bit == 0)                             bit_count++;         // previous bit was 0
                        else {                                                              // previous bit was 1
                            if ((bit_count>=3)&&(bit_count<=8))        data[ind++] = 1;                     // singe bit
                            else if((bit_count>=9)&&(bit_count<=14))   {data[ind++] = 1; data[ind++] = 1;}  // double bit
                            else {
                                //data[ind++] = 4;                                          // debugging: error code 4
                                //data[ind++] = bit_count;                                  // debugging
                                reset();
                            }
                            prev_bit = 0;                                                   // change to current state
                            bit_count = 1; } }
                    else if ((pulse_count==7)||(pulse_count==10)||(pulse_count==13)) {      // current bit is undefined
                        bit_count++; }
                    else    {
                        //data[ind++] = 2;                                                  // debugging error code 2
                        //data[ind++] = pulse_count;                                        // debugging
                        reset();
                    }
                }
                else if (ind == (maxbuf-1)) {               // process last bit
                    if ((pulse_count==11)||(pulse_count==12))       data[ind++] = 1;         // current bit is 1
                    else if ((pulse_count==8)||(pulse_count==9))    data[ind++] = 0;         // current bit is 0
                    else if ((pulse_count==7)||(pulse_count==10)||(pulse_count==13)) {       // current bit is undefined
                        data[ind] = data[ind-1];
                        ind++; }
                    else    {
                        //data[ind++] = 2;                                                    // debugging error code 2
                        //data[ind++] = pulse_count;                                          // debugging
                        reset();
                    }
                }
            }
            else {                                          // looking for starting sequence of high bits (>14)
                if ((pulse_count == 11)||(pulse_count == 12)) {                               // current bit is 1
                    if (prev_bit == 1)      bit_count++;
                    else {
                        prev_bit = 1;
                        bit_count = 1;
                    }
                }
                else if ((pulse_count == 8)||(pulse_count == 9)) {                          // current bit is 0
                    if (prev_bit == 1) {
                        if (bit_count > 14)     acquisition_flag = 1; }                     // found the starting sequence!
                    prev_bit = 0;
                    bit_count = 1;
                }
                else if ((pulse_count == 6)||(pulse_count == 10)||(pulse_count == 13)) {    // current bit is undefined (assume the same as previous)
                    if (prev_bit == 1)      bit_count++;
                    else                    bit_count = 0;
                }
            }
            pulse_count = 1;
        }
    }
}


void initIO(void) {
    PLLCSR = 1<<PCKE | 1<<PLLE;                             // 64 MHz PLL for Timer 1
    TCCR1 = 1<<PWM1A | 1<<COM1A1 | 1<<CS11;                 // PWM A enable, OC1A clear on compare match, 1/2 prescale
    OCR1A = 0x80;                                           // 50% duty (PB1 output)
    
    DDRB = 1<<SSD1306_SCL | 1<<SSD1306_SDA | 1<<LED_PIN;
    
    _delay_ms(40);
    clk_div_8();                                            // slow down system clock
    ssd1306_init();                                         // I2C OLED display initialization
    ssd1306_clear();
    ssd1306_setpos(10, 0);
    ssd1306_string_font6x8("Ready to scan");
    clk_div_1();
    
    pwm(1);                                                 // enable PWM output and interrupt
}

uint8_t decode_manchester(void) {                              // deciper manchester encoding
    for(uint8_t i = 0, final_ind = 0; i < 90; i += 2, final_ind++) {
        if((data[i] == 1) && (data[i+1] == 0))                data[final_ind] = 1;          // 10 -> 1
        else if(data[i] == 0 && data[i+1] == 1)               data[final_ind] = 0;          // 01 -> 0
        else  {                                                                             // error
            _delay_ms(10);
            //transmit(255);                                                                // debug error code 255
            //_delay_ms(10);
            return 0;
        }
    }
    return 1;
}

void output_i2c(void) {
	PORTB |= 1<<LED_PIN;                                                // LED on
    
    /* Split the 45 bits captured into separate fields in accordance with the 26-bit "standard" format
     Card format:
     00000001-00000000001-P-FFFFFFFF-NNNNNNNNNNNNNNNN-P
     P = Parity, F = Facility code (0 to 255), N = Card Number (0 to 65,535)
     Parity mask (applies to 26 bits only):
     E-XXXXXXXXXXXX-YYYYYYYYYYYY-O
     E = Even Parity, O = Odd Parity, X = Even parity mask, Y = Odd parity mask */
 
    uint8_t tag_parity_even = data[19];
    uint8_t tag_parity_odd = data[44];
    uint8_t tag_site = 0;
    for (uint8_t i = 20; i<28; i++) tag_site |= data[i]<<(27-i);
    uint16_t tag_id = 0;
    for (uint8_t i = 28; i<44; i++) tag_id |= data[i]<<(43-i);

    clk_div_8();
    ssd1306_clear();
    ssd1306_setpos(1, 0);
    ssd1306_string("Site: ");
    ssd1306_numdecp(tag_site);
    ssd1306_setpos(1, 2);
    ssd1306_string("ID:    ");
    ssd1306_numdecp(tag_id);
    ssd1306_setpos(1, 4);
     ssd1306_string("Parity 1: ");
    ssd1306_numdecp(tag_parity_even);
    ssd1306_setpos(1, 6);
    ssd1306_string("Parity 2: ");
    ssd1306_numdecp(tag_parity_odd);
    clk_div_1();
    _delay_ms(500);
    PORTB &= ~1<<LED_PIN;                                               // LED off
}

int main(void) {
	initIO();

	while (1) {
        if(ind >= maxbuf) {
            pwm(0);
            //output_uart(90);                              // debugging: output deduplicated bit stream
            //_delay_ms(100);
         
            if (decode_manchester())    output_i2c();       // decode manchester and send to LED display if there are no errors
            //output_uart(45);                              // debugging: output manchester decoded bit stream
            //_delay_ms(100);
            
            reset();
            for (uint8_t i = 1; i < maxbuf; i++) data[i] = 0;
            pwm(1);
        }
        else _delay_ms(10);                                 // continue acquisition
	}
	return 0; // never reached
}


